package com.example.aws.springbootdynamodbcrud.entity;

import lombok.Data;

@Data
public class StudentDTO {

    private Long studentId;
    private String firstName;
    private String lastName;

}
