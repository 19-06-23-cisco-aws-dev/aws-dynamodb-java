package com.example.aws.springbootdynamodbcrud.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.aws.springbootdynamodbcrud.entity.StudentDTO;
import com.example.aws.springbootdynamodbcrud.service.StudentService;

@RestController
@RequestMapping("/api/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/")
    public StudentDTO createNewStudent(@RequestBody StudentDTO dto){
        return studentService.createNewStudent(dto);
    }
    
    @GetMapping("/{id}")
    public StudentDTO fetchStudent(@PathVariable("id") long id){
        return studentService.fetchStudent(id);
    }
    

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable("id") long id){
        studentService.deleteStudent(id);
    }
}