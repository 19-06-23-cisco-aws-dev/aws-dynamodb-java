package com.example.aws.springbootdynamodbcrud.service;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.example.aws.springbootdynamodbcrud.config.DynamoDbConfig;
import com.example.aws.springbootdynamodbcrud.entity.Student;
import com.example.aws.springbootdynamodbcrud.entity.StudentDTO;

@Service
public class StudentService {

    private final DynamoDBMapper dynamoDBMapper;

    private final DynamoDbConfig dynamoDbConfig;

    public StudentService(DynamoDBMapper dynamoDBMapper, DynamoDbConfig dynamoDbConfig) {
        this.dynamoDBMapper = dynamoDBMapper;
        this.dynamoDbConfig = dynamoDbConfig;
    }

    public StudentDTO createNewStudent(StudentDTO dto) {

        Student student = new Student();
        BeanUtils.copyProperties(dto, student);

        dynamoDBMapper.save(student);

        StudentDTO response = new StudentDTO();
        BeanUtils.copyProperties(student,response);

        return response;
    }
    
    public StudentDTO fetchStudent(long id){
    	Student student = dynamoDBMapper.load(Student.class, id);
        StudentDTO response = new StudentDTO();

    	BeanUtils.copyProperties(student,response);
        return response;
    }
    
    public void deleteStudent(long id){
    	Student student = dynamoDBMapper.load(Student.class, id);
    	dynamoDBMapper.delete(student);
    }
}